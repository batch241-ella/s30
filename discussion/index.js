// MongoDB: Aggregation and Query Case Studies
/*
	- MongoDB Aggregation is used to generate manipulated data and perform operations to create filtered results that helps in analyzing data.
	- Compared to doing CRUD operations, aggregation gives us access to manipulate, filter, and compute for results. Providing us with information to make necessary development without having to create a frontend application.
*/

// Using the aggregated method
/*
	- The "$match" is used to pass the documents that meet the specified conditions(s) to the next pipeline stage / aggregation process.
	- Syntax:
		- { $match: { field: value } }
		- { $group: { _id: "value", fieldResult: "valueResult" } }

	- Using both "$match" and "$group" along with aggregation will find for products that are on sale and will group all stocks for all suppliers found.

		- db.collectionName.aggregate([
			 $match: { fieldA: valueA } }
			{ $group: { _id: "fieldB", fieldResult: {operation} } }

		])
*/
db.fruits.aggregate([
	{$match: {onSale: true}},
	{$group: {_id: "$supplier_id", total: {$sum: "$stock"}}}
]);


// Field projection with aggregation
/*
	- The "$project" can be used when aggregating data to include / exclude fields from the returned results.
	- Syntax:
		- { $project: { field 1 / 0 } }
*/
db.fruits.aggregate([
	{$match: {onSale: true}},
	{$group: {_id: "$supplier_id", total: {$sum: "$stock"}}},
	{$project: {_id: 0}}
]);


// Sorting aggregated results
/*
	- The "$sort" can be used to change the order of aggregated results
	- Use -1 to reverse the order
	- Syntax:
		- {$sort: {field 1 / -1}}
*/
db.fruits.aggregate([
	{$match: {onSale: true}},
	{$group: {_id: "$supplier_id", total: {$sum: "$stock"}}},
	{$sort: {total: -1}}
]);


// Aggregating results based on array fields
/*
	- The "$unwind" deconstructs an array field from a collection / field with an array value to output a result for each element
	- The syntax below will return results, creating seperate documents for each of the countries provided per the "origin" field
	- Syntax:
		- {$unwind: "$field"}
*/
db.fruits.aggregate([
	{$unwind: "$origin"}
]);

// Display fruit documents by their origin and the kind of fruits that are supplied
db.fruits.aggregate([
	{$unwind: "$origin"},
	{$group: {_id: "$origin", kinds: {$sum: 1}}}
]);